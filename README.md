# Outline

* Create a docker image which can run "ping" from the Dockerfile.
* Run ping command on the container created from the docker image.
* Push the docker image to a container registry. 

# How to use

1. Git clone.
    ~~~
    > git clone https://gitlab.com/naonobu.nakashima/docker_demo_build.git
    ~~~
1. Build docker Image from Dockerfile.
    ~~~
    > docker image build -t registry.gitlab.com/naonobu.nakashima/docker_demo/docker_build:0.1 .
    ~~~
1. Docker run. Execute ping command on the container.
    ~~~
    > docker run -it --rm registry.gitlab.com/naonobu.nakashima/docker_demo/docker_build:0.1 ping localhost
    ~~~
1. Docker push.
    ~~~
    > docker login registry.gitlab.com/naonobu.nakashima/docker_demo/
    > docker push registry.gitlab.com/naonobu.nakashima/docker_demo/docker_build:0.1
    ~~~